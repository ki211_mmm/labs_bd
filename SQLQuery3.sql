use productBD

--lab1
create table ProductBrands(
	Id int identity(1,1) primary key, 
	Name varchar(20) not null,
	code int,
	cost decimal(7, 3)
)

create table Consumers(
	Id int identity(1,1) primary key,
	Name varchar(20) not null,
	Address varchar(50) not null,
	Phone varchar(20) not null,
	BankAccount varchar(25) not null
)
create table Orders(
	Id int identity(1,1) primary key,
	OCount int not null check(OCount > 0),
	DeliveryMonth date default getdate(),
	enterpriseName varchar(30) not null, 
	ProductBrandId int foreign key(ProductBrandId) references ProductBrands(Id),
	ConsumerId int foreign key(ConsumerId) references Consumers(Id),
)
create table Shipments(
	Id int identity(1,1) primary key,
	dateOfShipmnet date default getdate(),
	Quantity int,
	OrderId int foreign key(OrderId) references Orders(Id),
	ConsumerId int foreign key(ConsumerId) references Consumers(Id)
)
create table RowsTotal(
	TableName varchar(20) not null,
	RowCnt int,
)

insert into ProductBrands
values('prod1', 1111, 1111),('prod2', 2222, 2222),('prod3', 3333, 3333),('prod4', 4444, 4444)
select * from ProductBrands

insert into Consumers
values('cons1', 'consadd1', '1111', '1111'),('cons2', 'consadd2', '2222', '2222'),('cons3', 'consadd3', '3333', '3333'),('cons4', 'consadd4', '4444', '4444')
select * from Consumers

insert into Orders
values(1, getdate(), 'entName5', 1, 1),(2, getdate(), 'entName2', 2, 2),(3, getdate(), 'entName3', 3, 3),(4, getdate(), 'entName4', 4, 4)
select * from Orders

insert into Shipments
values(getdate(), 1, 1, 1),(getdate(), 2, 2, 2),(getdate(), 3, 3, 3),(getdate(), 4, 4, 4)
select * from Shipments

SELECT * FROM Consumers;

SELECT Orders.ProductBrandId, ProductBrands.Name AS BrandName, SUM(Shipments.Quantity) AS
TotalQuantity
FROM Orders
INNER JOIN ProductBrands ON Orders.ProductBrandId = ProductBrands.Id
LEFT JOIN Shipments ON Orders.Id = Shipments.OrderId
WHERE YEAR(Orders.DeliveryMonth) = YEAR(GETDATE())
GROUP BY Orders.ProductBrandId, ProductBrands.Name;

SELECT Shipments.Id, Orders.DeliveryMonth, Consumers.Address
FROM Shipments
INNER JOIN Orders ON Shipments.OrderId = Orders.Id
INNER JOIN Consumers ON Orders.ConsumerId = Consumers.Id;

SELECT Orders.ConsumerId, Consumers.Name AS ConsumerName, Orders.ProductBrandId,
ProductBrands.Name AS BrandName, COUNT(*) AS UnfulfilledQuantity
FROM Orders
INNER JOIN Consumers ON Orders.ConsumerId = Consumers.Id
INNER JOIN ProductBrands ON Orders.ProductBrandId = ProductBrands.Id
LEFT JOIN Shipments ON Orders.Id = Shipments.OrderId
WHERE Shipments.Id IS NULL
GROUP BY Orders.ConsumerId, Consumers.Name, Orders.ProductBrandId, ProductBrands.Name;

GO
CREATE PROCEDURE CountRows
AS
BEGIN
DELETE FROM RowsTotal;
 DECLARE @ConsumerCount INT;
 SELECT @ConsumerCount = COUNT(*) FROM Consumers;
 DECLARE @OrderCount INT;
 SELECT @OrderCount = COUNT(*) FROM Orders;
 DECLARE @ProductBrandCount INT;
 SELECT @ProductBrandCount = COUNT(*) FROM ProductBrands;
 DECLARE @ShipmentCount INT;
 SELECT @ShipmentCount = COUNT(*) FROM Shipments;
 INSERT INTO RowsTotal (TableName, RowCnt)
 VALUES ('Consumers', @ConsumerCount),
 ('Orders', @OrderCount),
 ('ProductBrands', @ProductBrandCount),
 ('Shipments', @ShipmentCount);
SELECT * FROM RowsTotal;
END;

EXEC CountRows

GO
CREATE PROCEDURE CountColumns
AS
BEGIN
 IF OBJECT_ID('ColumnCount', 'U') IS NOT NULL
 DROP TABLE ColumnCount;
 CREATE TABLE ColumnCount (
 TableName NVARCHAR(50),
 ColumnCnt INT
 );
 INSERT INTO ColumnCount (TableName, ColumnCnt)
 SELECT 'Orders', COUNT(*)
 FROM INFORMATION_SCHEMA.COLUMNS
 WHERE TABLE_NAME = 'Orders'
 UNION ALL
 SELECT 'Consumers', COUNT(*)
 FROM INFORMATION_SCHEMA.COLUMNS
 WHERE TABLE_NAME = 'Consumers'
 UNION ALL
 SELECT 'Shipments', COUNT(*)
 FROM INFORMATION_SCHEMA.COLUMNS
 WHERE TABLE_NAME = 'Shipments'
 UNION ALL
 SELECT 'ProductBrands', COUNT(*)
 FROM INFORMATION_SCHEMA.COLUMNS
 WHERE TABLE_NAME = 'ProductBrands';
 SELECT * FROM ColumnCount;
END;

EXEC CountColumns

GO
CREATE PROCEDURE CountDistinctValues
 @TableName NVARCHAR(50)
AS
BEGIN
 DECLARE @Columns TABLE (ColumnName NVARCHAR(50));
 INSERT INTO @Columns (ColumnName)
 SELECT COLUMN_NAME
 FROM INFORMATION_SCHEMA.COLUMNS
 WHERE TABLE_NAME = @TableName;
IF OBJECT_ID('DistinctValueCount', 'U') IS NOT NULL
DROP TABLE DistinctValueCount;
 -- ��������� ������� ��� ��������� ����������
 CREATE TABLE DistinctValueCount (
 ColumnName NVARCHAR(50),
 DistinctValueCnt INT
 );
 DECLARE @ColumnName NVARCHAR(50);
 DECLARE @SqlQuery NVARCHAR(MAX);
 WHILE EXISTS (SELECT * FROM @Columns)
 BEGIN
 SELECT TOP 1 @ColumnName = ColumnName FROM @Columns;
 SET @SqlQuery = 'INSERT INTO DistinctValueCount (ColumnName, DistinctValueCnt)
 SELECT ''' + @ColumnName + ''', COUNT(DISTINCT ' + @ColumnName + ')
 FROM ' + @TableName;
 EXECUTE sp_executesql @SqlQuery;
 DELETE FROM @Columns WHERE ColumnName = @ColumnName;
 END;
 SELECT * FROM DistinctValueCount;
END;

EXEC CountDistinctValues @TableName = 'ProductBrands'

GO
CREATE TRIGGER UpdateTwo on Orders AFTER UPDATE AS
BEGIN
declare @ordersId int = (select Id from inserted)
UPDATE Orders
set OCount = 7
where Orders.Id = @ordersId

UPDATE RowsTotal
    SET RowCnt = (SELECT COUNT(*) FROM Orders);

SELECT * FROM Orders
SELECT * FROM RowsTotal
END;

GO
CREATE TRIGGER DeletePatient on Orders AFTER DELETE AS
BEGIN
DECLARE @OrderId int = (SELECT Id from deleted);
DELETE
FROM Shipments
WHERE OrderId = @OrderId
SELECT * FROM Shipments
SELECT * FROM Orders
END

--lab2-3
Select @@SERVERNAME as [Server\Instance];
Select @@VERSION as SQLServerVersion;
Select @@ServiceName AS ServiceInstance;
Select DB_NAME() AS CurrentDB_Name;
EXEC sp_helpserver;
EXEC sp_helpdb;

SELECT *
FROM sys.objects
WHERE type = 'U';
EXEC sp_Helpfile

EXEC sp_tables;
SELECT 'Select ''' + DB_NAME() + '.' + SCHEMA_NAME(SCHEMA_ID) + '.'
 + LEFT(o.name, 128) + ''' as DBName, count(*) as Count From ' + SCHEMA_NAME(SCHEMA_ID)
+ '.' + o.name
 + ';' AS ' Script generator to get counts for all tables'
FROM sys.objects o
WHERE o.[type] = 'U'
ORDER BY o.name;
SELECT @@ServerName AS ServerName ,
 DB_NAME() AS DBName ,
 OBJECT_NAME(ddius.object_id) AS TableName ,
 SUM(ddius.user_seeks + ddius.user_scans + ddius.user_lookups)
 AS Reads ,
 SUM(ddius.user_updates) AS Writes ,
 SUM(ddius.user_seeks + ddius.user_scans + ddius.user_lookups
 + ddius.user_updates) AS [Reads&Writes] ,
 ( SELECT DATEDIFF(s, create_date, GETDATE()) / 86400.0
 FROM master.sys.databases
 WHERE name = 'tempdb'
 ) AS SampleDays ,
 ( SELECT DATEDIFF(s, create_date, GETDATE()) AS SecoundsRunnig
 FROM master.sys.databases
 WHERE name = 'tempdb'
 ) AS SampleSeconds
FROM sys.dm_db_index_usage_stats ddius
 INNER JOIN sys.indexes i ON ddius.object_id = i.object_id
 AND i.index_id = ddius.index_id
WHERE OBJECTPROPERTY(ddius.object_id, 'IsUserTable') = 1
 AND ddius.database_id = DB_ID()
GROUP BY OBJECT_NAME(ddius.object_id)
ORDER BY [Reads&Writes] DESC;

SELECT @@Servername AS ServerName ,
 DB_NAME() AS DBName ,
 Name AS ViewName ,
 create_date
FROM sys.Views
ORDER BY Name

SELECT @@Servername AS ServerName ,
 DB_NAME() AS DBName ,
 o.name AS StoredProcedureName ,
 o.[Type] ,
 o.create_date
FROM sys.objects o
WHERE o.[Type] = 'P' -- Stored Procedures
ORDER BY o.name

SELECT @@Servername AS ServerName ,
 DB_NAME() AS DB_Name ,
 o.name AS 'Functions' ,
 o.[Type] ,
 o.create_date
FROM sys.objects o
WHERE o.Type = 'FN' -- Function
ORDER BY o.NAME;

--lab4

create table test_index (
 id int not null,
 pole1 char (36) not null,
 pole2 char (216) not null
)

select OBJECT_NAME ( object_id ) as table_name ,
name as index_name , type , type_desc
from sys . indexes
where OBJECT_ID = OBJECT_ID ( N'test_index ')

exec dbo.sp_spaceused @objname= N'test_index', @updateusage = true;

insert into test_index
values ( 1 , ' a' , 'b ' )

declare @i as int = 31
while @i < 240
begin
set @i = @i + 1;
insert into test_index
values ( @i , ' a' , 'b ' )
end;

insert into test_index
values ( 31 , ' a' , 'b ' )

insert into test_index
values ( 241 , ' a' , 'b ' )

truncate table test_index
create clustered index idx_cl_id on test_index ( id )

declare @a as int = 0
while @a < 18630
begin
set @a = @a + 1 ;
insert into test_index
values ( @a , ' a' , 'b ' )
end;

insert into test_index
values ( 18631 , ' a' , 'b ' )

truncate table test_index
declare @e as int = 0
while @e < 8906
begin
set @e = @e + 1 ;
insert into test_index
values ( @e % 100 , ' a' , 'b ' )
end ;

drop index idx_cl_pole1 on test_index
create nonclustered index idx_ncl_pole1 on test_index ( pole1 )
truncate table test_index

declare @q as int = 0
while @q < 24472
begin
set @q = @q + 1 ;
insert into test_index
values ( @q , format ( @q , '0000' ), 'b' )
end;

insert into test_index
values ( 24473 , ' 000024473' , 'b ' )

drop index idx_ncl_id on test_index
create clustered index idx_cl_pid on test_index ( id )
create nonclustered index idx_ncl_pole1 on test_index ( pole1 )

truncate table test_index
declare @r as int = 0
while @r < 28864
begin
set @r = @r + 1 ;
insert into test_index
values ( @r , format ( @r , '0000' ), 'b' )
end ;

insert into test_index
values ( 28865 , ' 000028865' , 'b ' )


SELECT sysobjects . name AS ������� , sysindexes . name AS ������ , sysindexes . indid
AS �����
FROM sysobjects INNER JOIN
sysindexes ON sysobjects . id = sysindexes . id
WHERE ( sysobjects . xtype = 'U' ) AND ( sysindexes . indid > 0 )
ORDER BY sysobjects . name , sysindexes . indid


create nonclustered index ix_code on ProductBrands(code)
with fillfactor = 70
select OBJECT_NAME ( object_id ) as table_name,
name as index_name , type , type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID ( N'ProductBrands')

drop index ix_consumers_bankAccount_phone on Consumers
create nonclustered index ix_consumers_bankAccount_phone on Consumers(BankAccount, Phone)
select OBJECT_NAME ( object_id ) as table_name,
name as index_name , type , type_desc
from sys.indexes
where OBJECT_ID = OBJECT_ID ( N'Consumers')

SELECT
    index_type_desc,
    page_count,
    record_count,
    avg_page_space_used_in_percent,
    avg_fragment_size_in_pages
FROM
    sys.dm_db_index_physical_stats(
        DB_ID(),
        OBJECT_ID(N'Consumers'),
        NULL,
        NULL,
        'DETAILED'
    );

--lab5
BACKUP DATABASE productBD
TO DISK = 'E:\labs\db\Backup\productBD_full.bak'
WITH FORMAT;

BACKUP DATABASE productBD
TO DISK = 'E:\labs\db\Backup\productBD_Differential.bak'
WITH DIFFERENTIAL, FORMAT;

BACKUP LOG productBD
TO DISK = 'E:\labs\db\Backup\productBD_Log.bak'
WITH FORMAT;

DROP DATABASE productBD

GO
USE master;
RESTORE DATABASE productBD
FROM DISK = 'E:\labs\db\Backup\productBD_full.bak'
WITH NORECOVERY;

GO
USE master;
RESTORE DATABASE productBD
FROM DISK = 'E:\labs\db\Backup\productBD_Differential.bak'
WITH NORECOVERY;

RESTORE LOG productBD
FROM DISK = 'E:\labs\db\Backup\productBD_Log.bak'